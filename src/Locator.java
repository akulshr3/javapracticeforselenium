import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.*;
//import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Locator {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		/*
		 * driver.get("http://facebook.com");
		 * driver.findElement(By.xpath("//input[@data-testid='royal_email']")).
		 * sendKeys("This is my email id"); //driver.close();
		 * driver.findElement(By.xpath("//input[@data-testid='royal_pass']")).sendKeys(
		 * "password"); driver.findElement(By.xpath("//*[text()='Log In']")).click();
		 * //driver.findElement(By.cssSelector("input[aria-label='Log In']")).click();
		 * //driver.findElement(By.linkText("Forgotten password?")).click();
		 */

		// driver.get("https://login.salesforce.com/?locale=in");
		// driver.findElement(By.id("username")).sendKeys("Salesforce username");
		// driver.findElement(By.name("pw")).sendKeys("password");
		// driver.findElement(By.id("Login")).click();

		/*
		 * driver.get("https://login.salesforce.com/?locale=in");
		 * driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("username");
		 * driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("password");
		 * driver.findElement(By.xpath("//*[@id=\"Login\"]")).click();
		 * System.out.println(driver.findElement(By.xpath("//*[@id=\'error\']")).getText
		 * ());
		 */

		/*
		 * driver.get("http://rediff.com");
		 * driver.findElement(By.cssSelector("a[title*='Sign in']")).click();
		 * driver.findElement(By.xpath("//input[@id='login1']")).sendKeys("username");
		 * driver.findElement(By.xpath("//input[@type='password']")).sendKeys("password"
		 * ); driver.findElement(By.cssSelector("input[type*='submit']")).click();
		 */

		// driver.get("https://rahulshettyacademy.com/#/index");
		// driver.findElement(By.xpath("/html/body/app-root/div/header/div[2]/div/div/div[2]/nav/div[2]/ul/li[4]/a")).click();
		// driver.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("Anand
		// Kulshreshtha");
		// driver.findElement(By.xpath("//*[@id='email']")).sendKeys("Anand.Kulshreshtha@gmail.com");
		// driver.findElement(By.xpath("//*[@id=\"form-submit\"]")).click();

		// h4[contains(@class,'broccoli')]
		// a[@class='increment']
		// button[@type='button']

		/*
		 * driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		 * driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[1]/h4")).
		 * getLocation(); driver.findElement(By.xpath(
		 * "//*[@id=\"root\"]/div/div[1]/div/div[1]/div[2]/a[2]")).click();
		 * driver.findElement(By.xpath(
		 * "//*[@id=\"root\"]/div/div[1]/div/div[1]/div[3]/button")).click();
		 * System.out.println(driver.findElement(By.xpath(
		 * "//*[@id=\"root\"]/div/header/div/div[3]/div[1]/table/tbody/tr[2]/td[3]/strong"
		 * )).getText());
		 */

		// driver.get("https://www.google.com/?gws_rd=ssl");
		// driver.manage().window().maximize();
		// driver.findElement(By.xpath("//a[contains(text(),'Images')]")).click();
		// driver.findElement(By.xpath("//a[@class='gb_D gb_tc']")).click();
		// driver.findElement(By.linkText("https://www.youtube.com/?gl=IN&amp;tab=i1")).click();
		// driver.findElement(By.xpath("//a[@class='tX9u1b']/div/span")).click();
		// driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")).sendKeys("SRK");
		// driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")).sendKeys(Keys.ENTER);

		// driver.findElement(By.xpath("//*[@id=\'tsf\']/div[2]/div[1]/div[3]/center/input[1]")).click();
		// driver.findElement(By.cssSelector("a[title*='Sign in']")).click();
		// driver.findElement(By.xpath("//input[@id='login1']")).sendKeys("username");
		// driver.findElement(By.xpath("//input[@type='password']")).sendKeys("password");
		// driver.findElement(By.cssSelector("input[type*='submit']")).click();*/

		/*
		 * driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		 * driver.manage().window().maximize();
		 * driver.findElement(By.xpath("//button[contains(text(),'Home')]")).click();
		 * driver.navigate().back(); driver.findElement(By.xpath(
		 * "//button[contains(text(),'Home')]/following-sibling::button[2]")).click();
		 * driver.navigate().back();
		 */

		// =================================================================================================

		// Below program will show how to use drop downs, radio buttons etc.

		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		// driver.manage().window().maximize();

		driver.findElement(By.xpath("//label[contains(text(),'Round Trip')]")).click();
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.className("picker-second")).getAttribute("style"));

		if (driver.findElement(By.className("picker-second")).getAttribute("style").contains("1"))

		{

			System.out.println("Round trip is enabled.");

		}

		// System.out.println(driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date2']")).isEnabled());
		// Assert.assertTrue(driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date2']")).isEnabled());

		driver.findElement(By.xpath("//table//label[contains(text(),'One Way')]")).click();
		System.out.println(driver.findElement(By.className("picker-second")).getAttribute("style"));

		if (driver.findElement(By.className("picker-second")).getAttribute("style").contains("0.5"))

		{
			System.out.println("One way is enabled");

		}

		// System.out.println(driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date2']")).isEnabled());
		// Assert.assertTrue(driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date2']")).isEnabled());

		driver.findElement(By.id("divpaxinfo")).click();
		Thread.sleep(2000L);
		// driver.findElement(By.id("hrefIncAdt")).click(); // this will add 2 adults.
		// But what if we want to add several adults.
		// This can be done by using a loop.

		int i = 1;
		while (i < 4) // this will click 3 times

		{
			driver.findElement(By.id("hrefIncAdt")).click();
			i++;
		}

		// for (i=1;i<4;i++) //same thing done using for loop this time.

		// {
		driver.findElement(By.id("hrefIncAdt")).click();
		// }

		driver.findElement(By.id("btnclosepaxoption")).click();
		System.out.println("You have selected " + driver.findElement(By.id("divpaxinfo")).getText() + "s");

		Assert.assertEquals(driver.findElement(By.id("divpaxinfo")).getText(), "5 Adult");

		Select s = new Select(driver.findElement(By.id("ctl00_mainContent_DropDownListCurrency")));
		// s.selectByVisibleText("USD");
		// s.selectByValue("INR");
		s.selectByIndex(2);

		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		driver.findElement(By.xpath("//a[@text='Bengaluru (BLR)']")).click();

		Assert.assertEquals(driver.findElement(By.xpath("//a[@text='Bengaluru (BLR)']")).getText(), "Bengaluru (BLR)");

		Thread.sleep(2000);
		driver.findElement(By.id("ctl00_mainContent_ddl_destinationStation1_CTXT")).click();
		driver.findElement(By.xpath("(//a[@value='MAA'])[2]")).click();

		driver.findElement(
				By.xpath("//body/form[@name='aspnetForm']/div/div/div/div/div/div/div/div/div/div[12]/div[1]")).click();
		System.out.println(driver
				.findElement(
						By.xpath("//body/form[@name='aspnetForm']/div/div/div/div/div/div/div/div/div/div[12]/div[1]"))
				.isDisplayed());

		// Using Calendars

		driver.findElement(By.cssSelector(".ui-datepicker-trigger")).click();
		// driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight")).click();
		driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/table[1]/tbody[1]/tr[5]/td[1]/span[1]")).click();

		// ===================================================================================

		/*
		 * driver.findElement(By.
		 * xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT'] //a[@value='BLR')]"
		 * )).click(); //driver.findElement(By.
		 * xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT'] //a[@value='BLR')]"
		 * )).click(); driver.findElement(By.xpath(
		 * "//input[@id='ctl00_mainContent_ddl_destinationStation1_CTXT']")).click();
		 * driver.findElement(By.
		 * xpath("//input[@id='ctl00_mainContent_ddl_destinationStation1_CTXT'] //a[@value='MAA')]"
		 * )).click();
		 */

		// Auto suggestive drop downs

		/*
		 * driver.get("https://www.makemytrip.com/");
		 * //driver.manage().window().maximize();
		 * driver.findElement(By.xpath("//div[@class='makeFlex']/ul/li[2]")).click(); //
		 * this selects Roundtrip radio button
		 * //System.out.println(driver.findElement(By.xpath(
		 * "//div[@class='makeFlex']/ul/li[2]")).isDisplayed());
		 * 
		 * Assert.assertTrue(driver.findElement(By.xpath(
		 * "//div[@class='makeFlex']/ul/li[2]")).isDisplayed()); // Assertion used.
		 * 
		 * 
		 * driver.findElement(By.xpath("//span[contains(text(),'From')]")).click();
		 * WebElement
		 * source=driver.findElement(By.xpath("//input[@placeholder='From']")); //web
		 * element is assigned to a variable of name source source.sendKeys("mumb");
		 * Thread.sleep(2000); source.sendKeys(Keys.ARROW_DOWN); Thread.sleep(1000);
		 * source.sendKeys(Keys.ENTER); Assert.assertEquals(source.getText(),
		 * "Mumbai, India");
		 * 
		 * driver.findElement(By.xpath("//span[contains(text(),'To')]")).click();
		 * driver.findElement(By.xpath("//input[@placeholder='To']")).sendKeys("del");
		 * Thread.sleep(2000);
		 * driver.findElement(By.xpath("//input[@placeholder='To']")).sendKeys(Keys.
		 * ARROW_DOWN); Thread.sleep(1000);
		 * driver.findElement(By.xpath("//input[@placeholder='To']")).sendKeys(Keys.
		 * ENTER);
		 * 
		 * 
		 * //
		 * Assert.assertEquals(driver.findElement(By.xpath("//input[@placeholder='To']")
		 * ).sendKeys("del"), "Delhi");
		 * 
		 * driver.findElement(By.xpath("//li[contains(text(),'Student Fare')]")).click()
		 * ;
		 * 
		 * driver.findElement(By.xpath("//span[contains(text(),'Travellers & CLASS')]"))
		 * .click();
		 * driver.findElement(By.xpath("//div[@class='travellers']/div/ul/li[4]")).click
		 * ();
		 * 
		 * System.out.println(driver.findElements(By.xpath(
		 * "//span[contains(@class,'tabsCircle')]")).size());
		 * Assert.assertEquals(driver.findElements(By.xpath(
		 * "//span[contains(@class,'tabsCircle')]")), 3);
		 */

	}

}
