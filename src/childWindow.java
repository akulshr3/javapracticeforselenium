import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class childWindow {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		driver.get("https://accounts.google.com/signup");
		driver.findElement(By.xpath("//a[contains(text(),'Help')]")).click();
				
	//	System.out.println(driver.getWindowHandles());
		
		Set <String> ids = driver.getWindowHandles();  //This will give us window handle ids.
	//	System.out.println(ids);
		
	//	driver.switchTo().window("48F431AD625DB2DEA6EC645FD0385D23");
	//	driver.switchTo().window(Ids(1));
		
		Iterator <String>  iter = ids.iterator();  //iterator is a class that is used here.
		String parentid=iter.next();  
		driver.switchTo().window(parentid);
		System.out.println(driver.getTitle());
		
	//	System.out.println(driver.getTitle());

		String childid=iter.next();
		driver.switchTo().window(childid);

		System.out.println(driver.getTitle());

	
	}

}
