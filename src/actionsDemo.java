import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class actionsDemo {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		int i =10;
		driver.get("https://www.amazon.com/");
		
		Actions act = new Actions (driver);
		
		//Below will just mouse over the element and open a pop up. It will not click anything.
		act.moveToElement(driver.findElement(By.xpath("//a[@id='nav-link-accountList']//span[2]"))).build().perform();
		
		//driver.findElement(By.id("twotabsearchtextbox"));
		act.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).click().keyDown(Keys.SHIFT).sendKeys("hello").doubleClick().contextClick().build().perform();
		
	}

}
