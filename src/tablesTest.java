import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class tablesTest {

	public static void main(String[] args) {
		
		int sum=0;

		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
		driver.get("https://www.cricbuzz.com/live-cricket-scorecard/22761/ind-vs-aus-2nd-odi-australia-tour-of-india-2020");
		
		WebElement table=driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr']")); //This will identify and select the 1st table only.
		int rowcount=table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms']")).size(); //This will give the row count in the table selected.
		
		int count = table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).size(); //This will give the count of values
																																// from 3rd column (runs)
		
		for (int i=0;i<count-2;i++) // count -2 will remove the last 2 iterations from the for loop.
		{
			String value =table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).get(i).getText();
			int runs=Integer.parseInt(value);
			sum=sum+runs;
			
		}
		
		System.out.println("sum of runs =" + sum);
		
		String extras=driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr'] div:nth-child(11) div:nth-child(2)")).getText();
		int extrarun=Integer.parseInt(extras);
		
		System.out.println("Extras = " + extrarun);
		
		sum=sum+extrarun;
		System.out.println("Total runs= " + sum);
		
		
	}

}
