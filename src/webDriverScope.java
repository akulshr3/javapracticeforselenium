import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class webDriverScope {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// Give me the count of the no of links on the page.

		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		System.out.println(driver.findElements(By.tagName("a")).size());

		WebElement footerdriver = driver.findElement(By.id("gf-BIG")); // Using this 'footerdriver' variable now has
																		// been created
																		// with its scope limited to footer section only
		System.out.println(footerdriver.findElements(By.tagName("a")).size());


		WebElement columndriver = footerdriver.findElement(By.xpath("//table/tbody/tr/td[1]/ul"));
		System.out.println("Below is the count of links in the 1st column");
		System.out.println(columndriver.findElements(By.tagName("a")).size());

	//	System.out.println(driver.findElement(By.xpath("//h1[contains(text(),'Practice Page')]")).getText());

		
		for (int i=1; i<columndriver.findElements(By.tagName("a")).size();i++)
		{
			
			String openlinkinnewtab=Keys.chord(Keys.CONTROL,Keys.ENTER);
			columndriver.findElements(By.tagName("a")).get(i).sendKeys(openlinkinnewtab);
			
			Thread.sleep(5000L);
		}
			Set <String> Ids = driver.getWindowHandles();
			Iterator <String> iter = Ids.iterator();
		
			while (iter.hasNext())
			{
				driver.switchTo().window(iter.next());
				System.out.println(driver.getTitle());
				
			}
			 
	
		//	System.out.println("Below is the count of links in the middle frame");
		//	driver.switchTo().frame(driver.findElement(By.id("courses-iframe"))); // Switched to frame in the middle
																					// section.
		//	System.out.println(driver.findElements(By.tagName("a")).size()); // Count of links in the frame in the middle
																				// section

		//	driver.switchTo().defaultContent();

		
		//		Actions act = new Actions(driver);
		//	act.moveToElement(driver.findElement(By.xpath("//h1[contains(text(),'Practice Page')]"))).doubleClick().build()
		//			.perform();
		// act.click(driver.findElement(By.xpath("//h1[contains(text(),'Practice
		// Page')]"))).doubleClick().build().perform();
		
		
		
		/*Set <String> ids = driver.getWindowHandles();  //This will give us window handle ids.
		Iterator <String>  iter = ids.iterator();  //iterator is a class that is used here.
		String childid=iter.next();  
		driver.switchTo().window(childid);
		System.out.println(driver.getCurrentUrl());		*/		
		
		
	}

}
