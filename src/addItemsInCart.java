import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class addItemsInCart {

	public static void main(String[] args) throws InterruptedException {
				
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);  // This is Implicit wait. 
		
			
		String itemsNeeded [] = {"Mushroom","Corn", "Mango"};
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		//driver.manage().window().maximize();
		//Thread.sleep(3000);
		
		addItems(driver, itemsNeeded);
		addItemsInCart veg = new addItemsInCart ();
		veg.addItems(driver, itemsNeeded);
		
	}
	
	public static  void addItems (WebDriver driver, String itemsNeeded []) throws InterruptedException
		
	{

		WebDriverWait wait = new WebDriverWait(driver, 5);  // This is explicit wait.
		
		int j=0;
		List<WebElement> products=driver.findElements(By.cssSelector("h4.product-name"));
		
		for (int i=0;i<products.size();i++)
		{
			
			String[] name = products.get(i).getText().split("-");
			String trimmedName = name[0].trim();
			//format it to meet with the names mentioned in the array. We need to trim - 1 Kg
			//Search whether the names returned are present in the array or not.
			//convert array to array list for easy search.
			
			List itemslist = Arrays.asList(itemsNeeded);
			
		
			if (itemslist.contains(trimmedName))
			
			{
				j++;
				
			driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
			
			if (trimmedName.contains("Mango"))
			{
				driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();				
			}
			
				
			//Thread.sleep(1000);
			//driver.findElement(By.xpath("//*[text()='ADD TO CART']")).click();
			
			if (j==itemsNeeded.length)
				
			{
				break;
			
			}
			
		}

				
		}
			driver.findElement(By.xpath("//a[@class='cart-icon']")).click();
			driver.findElement(By.xpath("//div[@class='action-block']/button")).click();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Enter promo code']")));
			
			driver.findElement(By.xpath("//input[@placeholder='Enter promo code']")).sendKeys("rahulshettyacademy");
			driver.findElement(By.xpath("//button[contains(text(),'Apply')]")).click();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='promoInfo']")));
			
			System.out.println(driver.findElement(By.xpath("//span[@class='promoInfo']")).getText());
			driver.findElement(By.xpath("//button[text()='Place Order']")).click();
			
			driver.findElement(By.xpath("//select[@style='width: 200px;']")).click();
			driver.findElement(By.xpath("//select[@style='width: 200px;']/option[contains(text(),'India')]")).click();
			driver.findElement(By.xpath("//input[@type='checkbox']")).click();
			driver.findElement(By.xpath("//button[text()='Proceed']")).click();
	}
}

